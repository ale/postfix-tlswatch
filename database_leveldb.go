package tlswatch

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"

	"github.com/jmhodges/levigo"
	"strings"
)

var (
	LruCacheSize    = 2 << 20
	BloomFilterSize = 10
)

type LevelDbDatabase struct {
	db     *levigo.DB
	cache  *levigo.Cache
	filter *levigo.FilterPolicy
}

func NewLevelDbDatabase(path string) *LevelDbDatabase {
	opts := levigo.NewOptions()
	cache := levigo.NewLRUCache(LruCacheSize)
	opts.SetCache(cache)
	filter := levigo.NewBloomFilter(BloomFilterSize)
	opts.SetFilterPolicy(filter)
	opts.SetCreateIfMissing(true)
	db, err := levigo.Open(path, opts)
	if err != nil {
		log.Fatal(err)
	}
	return &LevelDbDatabase{db, cache, filter}
}

func (db *LevelDbDatabase) Close() {
	db.db.Close()
	db.cache.Close()
	db.filter.Close()
}

func (db *LevelDbDatabase) Session() Session {
	snap := db.db.NewSnapshot()
	ro := levigo.NewReadOptions()
	ro.SetSnapshot(snap)
	return &levelDbSession{
		db:       db.db,
		snap:     snap,
		readOpts: ro,
	}
}

type levelDbSession struct {
	db       *levigo.DB
	readOpts *levigo.ReadOptions
	snap     *levigo.Snapshot
	wb       *levigo.WriteBatch
}

func (s *levelDbSession) makeKey(table, key string) []byte {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "%s:%s", table, key)
	return buf.Bytes()
}

func (s *levelDbSession) Get(table, key string, obj interface{}) bool {
	data, err := s.db.Get(s.readOpts, s.makeKey(table, key))
	if err != nil || data == nil {
		return false
	}
	if obj != nil {
		gob.NewDecoder(bytes.NewReader(data)).Decode(obj)
	}
	return true
}

func (s *levelDbSession) Set(table, key string, obj interface{}) {
	if s.wb == nil {
		s.wb = levigo.NewWriteBatch()
	}

	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(obj); err != nil {
		return
	}

	s.wb.Put(s.makeKey(table, key), buf.Bytes())
}

func (s *levelDbSession) Del(table, key string) {
	if s.wb == nil {
		s.wb = levigo.NewWriteBatch()
	}

	s.wb.Delete(s.makeKey(table, key))
}

func (s *levelDbSession) Scan(table, startKey, endKey string) (Iterator, error) {
	iter := s.db.NewIterator(s.readOpts)
	iter.Seek(s.makeKey(table, startKey))
	return &levelDbIterator{
		iter:   iter,
		endKey: s.makeKey(table, endKey),
	}, nil
}

func (s *levelDbSession) Close() {
	if s.wb != nil {
		if err := s.db.Write(levigo.NewWriteOptions(), s.wb); err != nil {
			log.Printf("LevelDB write error: %s", err)
		}
		s.wb.Close()
	}
	s.db.ReleaseSnapshot(s.snap)
}

type levelDbIterator struct {
	iter                     *levigo.Iterator
	endKey, curValue []byte
	curKey string
}

func dropTable(key string) string {
	if pos := strings.Index(key, ":"); pos > 0 {
		return key[pos + 1:len(key)]
	}
	return key
}

func (i *levelDbIterator) Next() bool {
	if i.iter.Valid() {
		key := i.iter.Key()
		i.curKey = dropTable(string(key))
		i.curValue = i.iter.Value()
		i.iter.Next()
		return bytes.Compare(key, i.endKey) < 0
	}
	return false
}

func (i *levelDbIterator) Key() string {
	return i.curKey
}

func (i *levelDbIterator) Value(obj interface{}) {
	gob.NewDecoder(bytes.NewReader(i.curValue)).Decode(obj)
}

func (i *levelDbIterator) Close() {
	i.iter.Close()
}
