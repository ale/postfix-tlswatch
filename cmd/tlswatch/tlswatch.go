package main

import (
	"flag"
	"log"
	"os"
	"syscall"
	"time"

	"git.autistici.org/ale/postfix-tlswatch"
)

var (
	dbPath           = flag.String("db", "/var/lib/postfix-tlswatch/db", "Database directory")
	tlsPolicy        = flag.String("policy", "ca-pinning", "TLS policy (strict | ca-pinning)")
	tlsPolicyMapFile = flag.String("tls-policy-map", "/etc/postfix/maps/tls_policy", "Location of the Postfix tls_policy_map file")
	updatePeriod     = flag.Duration("update-period", 900*time.Second, "Update period")
	whitelistFile    = flag.String("whitelist", "", "Domain whitelist file")
	blacklistFile    = flag.String("blacklist", "", "Domain blacklist file")
	fifoPath         = flag.String("fifo", "", "Read data from a FIFO (run as daemon)")
)

func runOnce(scanner *tlswatch.Scanner) {
	// If any command-line arguments are specified, process them,
	// otherwise read from standard input.
	if flag.NArg() > 0 {
		for _, filename := range flag.Args() {
			f, err := os.Open(filename)
			if err != nil {
				log.Printf("Error opening '%s': %s", filename, err)
				continue
			}
			if err := scanner.Scan(f); err != nil {
				log.Printf("Error processing '%s': %s", filename, err)
			}
			f.Close()
		}
	} else {
		if err := scanner.Scan(os.Stdin); err != nil {
			log.Fatal(err)
		}
	}
}

func runFifo(scanner *tlswatch.Scanner) {
	// Create the FIFO if it does not exist, but raise a fatal
	// error if it already exists and it's a standard file.
	if info, err := os.Stat(*fifoPath); err == nil {
		if info.Mode()&os.ModeNamedPipe != os.ModeNamedPipe {
			log.Fatalf("%s is not a FIFO", *fifoPath)
		}
	} else {
		if err := syscall.Mkfifo(*fifoPath, 0770); err != nil {
			log.Fatal(err)
		}
	}

	// Outer loop on reading from *fifoPath to ignore EOFs.
	for {
		f, err := os.Open(*fifoPath)
		if err != nil {
			log.Fatal(err)
		}

		if err := scanner.Scan(f); err != nil {
			log.Fatal(err)
		}

		f.Close()
	}
}

func main() {
	flag.Parse()

	ch := make(chan string, 100)

	// Setup the interface to Postfix.
	pmap := tlswatch.NewPostfixPolicyMap(*tlsPolicyMapFile)
	policy, err := tlswatch.GetTlsPolicy(*tlsPolicy)
	if err != nil {
		log.Fatal(err)
	}

	// Initialize the database.
	db := tlswatch.NewLevelDbDatabase(*dbPath)
	defer db.Close()

	// Create the main PolicyWatcher object.
	pw := tlswatch.NewPolicyWatcher(
		policy,
		pmap,
		db,
		tlswatch.Batch(ch, *updatePeriod))

	// Setup the Scanner.
	var domainWl tlswatch.RegexpList
	if *whitelistFile != "" {
		domainWl = tlswatch.ParseWildcardsFromFile(*whitelistFile)
	}
	var domainBl tlswatch.RegexpList
	if *blacklistFile != "" {
		domainBl = tlswatch.ParseWildcardsFromFile(*blacklistFile)
	}

	scanner := tlswatch.NewScanner(db, ch, domainWl, domainBl)

	if *fifoPath != "" {
		runFifo(scanner)
	} else {
		runOnce(scanner)
	}

	close(ch)
	pw.Wait()
	pmap.Save()
}
