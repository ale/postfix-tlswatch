package tlswatch

// The Database holds information about known domain -> mx mappings
// and the related mx connection data. Every association has a
// freshness metric, to detect configuration changes by allowing old
// relations to expire.
type Database interface {
	Session() Session
	Close()
}

// Iterator is a database iterator to scan a result range.
type Iterator interface {
	Next() bool
	Key() string
	Value(interface{})
	Close()
}

// Session provides a consistent view of the database. Note that
// writes might not be immediately visible within the same Session.
type Session interface {
	Get(table, key string, obj interface{}) bool
	Set(table, key string, obj interface{})
	Del(table, key string)
	Scan(table, startKey, endKey string) (Iterator, error)
	Close()
}
