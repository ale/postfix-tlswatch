package tlswatch

import (
	"bytes"
	"crypto/sha1"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"log"
	"os"
	"net"
	"net/textproto"
	"sync"
	"time"

	"github.com/golang/groupcache/singleflight"
)

var (
	// Command-line flags.
	defaultLocalName string
	localName = flag.String("ehlo", "", "EHLO host (defaults to local hostname)")
	certInfoTTL = flag.Duration("cert-ttl", 7 * 24 * time.Hour, "TTL for certificate cache")
	certInfoErrTTL = flag.Duration("cert-err-ttl", 12 * time.Hour, "TTL for certificate errors cache")

	// SMTP connection timeout.
	connectTimeout = 30 * time.Second

	// Database table for the certificate cache.
	certsTable = "certs"
)

func init() {
	defaultLocalName, _ = os.Hostname()
}

// Certificate information used by the certificate cache.
type CertInfo struct {
	Addr    string
	Certs   []*x509.Certificate
	Err     error
	Expires time.Time
}

func (c CertInfo) Expired() bool {
	return time.Now().After(c.Expires)
}

func (c CertInfo) Equal(c2 CertInfo) bool {
	if c.Addr != c2.Addr || c.Err != c2.Err {
		return false
	}
	if len(c.Certs) != len(c2.Certs) {
		return false
	}
	for idx, crt := range c.Certs {
		if !crt.Equal(c2.Certs[idx]) {
			return false
		}
	}
	return true
}

// Send a SMTP command and return the response.
func smtpCmd(t *textproto.Conn, expectCode int, format string, args ...interface{}) (int, string, error) {
	id, err := t.Cmd(format, args...)
	if err != nil {
		return 0, "", err
	}
	t.StartResponse(id)
	defer t.EndResponse(id)
	code, msg, err := t.ReadResponse(expectCode)
	return code, msg, err
}

// GrabCerts returns the X509 certificates served by the remote
// address, using the STARTTLS SMTP extension.
func GrabCerts(addr string) ([]*x509.Certificate, error) {
	conn, err := net.DialTimeout("tcp", addr, connectTimeout)
	if err != nil {
		log.Printf("%s: connect error: %s", addr, err)
		return nil, err
	}
	defer conn.Close()

	tconn := textproto.NewConn(conn)
	defer tconn.Close()

	if _, _, err := tconn.ReadResponse(220); err != nil {
		log.Printf("%s: bad salutation: %s", addr, err)
		return nil, err
	}

	// Send EHLO, ignore reply.
	ehlo := *localName
	if ehlo == "" {
		ehlo = defaultLocalName
	}
	if _, _, err := smtpCmd(tconn, 250, "EHLO %s", ehlo); err != nil {
		log.Printf("%s: EHLO error: %s", addr, err)
		return nil, err
	}

	// Send STARTTLS.
	if _, _, err := smtpCmd(tconn, 220, "STARTTLS"); err != nil {
		log.Printf("%s: STARTTLS error: %s", addr, err)
		return nil, err
	}

	// Establish the TLS connection, and run the initial handshake.
	tlsConn := tls.Client(conn, &tls.Config{InsecureSkipVerify: true})
	defer tlsConn.Close()
	if err := tlsConn.Handshake(); err != nil {
		log.Printf("%s: TLS handshake error: %v", addr, err)
		return nil, err
	}

	return tlsConn.ConnectionState().PeerCertificates, nil
}

type grabCertResponse struct {
	certs []*x509.Certificate
	err   error
}

type grabCertRequest struct {
	addr string
	ch   chan grabCertResponse
}

// CertGrabber will retrieve X509 certificates from remote mail
// servers (keeping a cache in the local database), with a limit on
// the number of concurrent requests.
type CertGrabber struct {
	db Database
	sg singleflight.Group
	wg sync.WaitGroup
	ch chan grabCertRequest
}

func NewCertGrabber(db Database, nworkers int) *CertGrabber {
	g := &CertGrabber{
		db: db,
		ch: make(chan grabCertRequest, 100),
	}
	for i := 0; i < nworkers; i++ {
		g.wg.Add(1)
		go g.grabber()
	}
	return g
}

func (g *CertGrabber) Close() {
	close(g.ch)
	g.wg.Wait()
}

func (g *CertGrabber) grabber() {
	for req := range g.ch {
		addr := net.JoinHostPort(req.addr, "25")
		log.Printf("connecting to %s", addr)
		certs, err := GrabCerts(addr)
		req.ch <- grabCertResponse{certs, err}
	}
	g.wg.Done()
}

func (g *CertGrabber) grabCert(addr string) ([]*x509.Certificate, error) {
	rch := make(chan grabCertResponse, 1)
	defer close(rch)
	g.ch <- grabCertRequest{addr, rch}
	resp := <-rch
	return resp.certs, resp.err
}

// GrabCert attempts to retrieve the X509 certificates served by a
// mail server. Results are cached in the database.
func (g *CertGrabber) GrabCert(addr string) ([]*x509.Certificate, error) {
	session := g.db.Session()
	defer session.Close()

	var certinfo CertInfo
	certinfoOk := session.Get(certsTable, addr, &certinfo)
	if certinfoOk && !certinfo.Expired() {
		return certinfo.Certs, certinfo.Err
	}

	// Wrap the connection with a singleflight.Group to avoid
	// multiple updates when a popular CertInfo expires.
	certs, err := g.sg.Do(addr, func() (interface{}, error) {
		certs, err := g.grabCert(addr)
		ttl := *certInfoTTL
		if err != nil {
			ttl = *certInfoErrTTL
		}
		newcertinfo := CertInfo{
			Addr:    addr,
			Certs:   certs,
			Err:     err,
			Expires: time.Now().Add(ttl),
		}
		session.Set(certsTable, addr, &newcertinfo)

		// Here we can detect certificate changes!
		if certinfoOk && !newcertinfo.Equal(certinfo) {
			log.Printf("detected certificate change on %s: %+v -> %+v", addr, certinfo, newcertinfo)
		}

		return certs, err
	})
	return certs.([]*x509.Certificate), err
}

// Postfix uses SHA1 fingerprints of the public key.
func fpEncode(data []byte) string {
	var buf bytes.Buffer
	for i, b := range data {
		if i > 0 {
			buf.WriteString(":")
		}
		fmt.Fprintf(&buf, "%02X", b)
	}
	return buf.String()
}

// GetCertFingerprint returns the SHA1 fingerprints of the given
// certificates, encoded as colon-separated hex strings.
func GetCertFingerprint(cert *x509.Certificate) string {
	h := sha1.New()
	h.Write(cert.RawSubjectPublicKeyInfo)
	return fpEncode(h.Sum(nil))
}

// Get the CA certificates in the list.
func GetCACertificates(certs []*x509.Certificate) []*x509.Certificate {
	cas := make([]*x509.Certificate, 0, len(certs))
	for _, cert := range certs {
		if cert.IsCA {
			cas = append(cas, cert)
		}
	}
	return cas
}
