package tlswatch

import (
	"io/ioutil"
	"os"
	"testing"
)

func testSimple(db Database, t *testing.T) {
	addr := "localhost"
	mxinfo := MxInfo{addr, true, "TLSv1.2", "ECDHE-RSA-RC4-SHA"}
	session := db.Session()
	session.Set("mx", addr, &mxinfo)
	session.Close()

	session = db.Session()
	defer session.Close()
	if session.Get("mx", "unknown", nil) {
		t.Error("Get(nonexisting) is true")
	}

	if !session.Get("mx", addr, nil) {
		t.Error("Get(nil) is false")
	}

	var mxinfo2 MxInfo
	if !session.Get("mx", addr, &mxinfo2) {
		t.Error("Get() is false")
	}
	if mxinfo2 != mxinfo {
		t.Error("Get() returned bad result:", mxinfo2)
	}
}

func testScan(db Database, t *testing.T) {
	session := db.Session()
	session.Set("table", "aa", "value1")
	session.Set("table", "bb", "value2")
	session.Set("table", "cc", "value3")
	session.Set("table", "dd", "value3")
	session.Close()

	session = db.Session()
	defer session.Close()
	iter, err := session.Scan("table", "bb", "dd")
	if err != nil {
		t.Fatal(err)
	}

	type kv struct {
		key, value string
	}
	results := make([]kv, 0)
	for iter.Next() {
		var s string
		iter.Value(&s)
		results = append(results, kv{iter.Key(), s})
	}

	expected := []kv{{"bb", "value2"}, {"cc", "value3"}}

	if len(results) != len(expected) {
		t.Fatal("Bad Scan() results:", results)
	}
	for i := 0; i < len(results); i++ {
		if results[i] != expected[i] {
			t.Fatalf("Bad Scan() results (pos %d): %+v", i, results[i])
		}
	}
}

func TestInMemoryDatabase_Simple(t *testing.T) {
	db := NewInMemoryDatabase()
	defer db.Close()
	testSimple(db, t)
}

func TestInMemoryDatabase_Scan(t *testing.T) {
	db := NewInMemoryDatabase()
	defer db.Close()
	testScan(db, t)
}

func TestLevelDbDatabase_Simple(t *testing.T) {
	dir, _ := ioutil.TempDir("", "test-leveldb-")
	defer os.RemoveAll(dir)
	db := NewLevelDbDatabase(dir)
	defer db.Close()
	testSimple(db, t)
}

func TestLevelDbDatabase_Scan(t *testing.T) {
	dir, _ := ioutil.TempDir("", "test-leveldb-")
	defer os.RemoveAll(dir)
	db := NewLevelDbDatabase(dir)
	defer db.Close()
	testScan(db, t)
}
