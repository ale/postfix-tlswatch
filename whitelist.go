package tlswatch

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

// A list of regular expressions.
type RegexpList []*regexp.Regexp

func NewRegexpList(exprs []string) RegexpList {
	rxs := make(RegexpList, 0, len(exprs))
	for _, expr := range exprs {
		rxs = append(rxs, regexp.MustCompile(expr))
	}
	return rxs
}

// Match returns true if 'domain' matches any of the regular
// expressions in the list.
func (rl RegexpList) Match(domain string) bool {
	for _, rx := range rl {
		if rx.MatchString(domain) {
			return true
		}
	}
	return false
}

func wildcardToPattern(s string) string {
	p := fmt.Sprintf("^%s$", strings.Replace(strings.Replace(s, ".", "\\.", -1), "*", ".*", -1))
	// Deal with the '*.example.com' special case, which should
	// also match 'example.com'.
	if strings.HasPrefix(p, "^.*\\.") {
		p = fmt.Sprintf("^(|.*\\.)%s", p[5:len(p)])
	}
	return p
}

// Parse a list of wildcard patterns, and return a list of
// corresponding compiled regular expressions.
func ParseWildcardList(domains []string) RegexpList {
	patterns := make([]string, 0, len(domains))
	for _, w := range domains {
		patterns = append(patterns, wildcardToPattern(w))
	}
	return NewRegexpList(patterns)
}

// Parse a list of wildcard patterns from a stream.
func ParseWildcardsFromStream(r io.Reader) RegexpList {
	wildcards := make([]string, 0)
	br := bufio.NewScanner(r)
	for br.Scan() {
		line := strings.TrimSpace(br.Text())
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}
		wildcards = append(wildcards, line)
	}
	// br.Err()
	return ParseWildcardList(wildcards)
}

// Parse a list of wildcard patterns from a file.
func ParseWildcardsFromFile(path string) RegexpList {
	file, err := os.Open(path)
	if err != nil {
		return nil
	}
	defer file.Close()
	return ParseWildcardsFromStream(file)
}
