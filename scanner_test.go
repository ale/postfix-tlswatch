package tlswatch

import (
	"fmt"
	"strings"
	"sync"
	"testing"
	"time"
)

var (
	scannerTestData = `
Jun 25 07:28:17 fnord postfix/smtp[4894]: Untrusted TLS connection established to smtp-fw-4101.amazon.com[72.21.198.25]:25: TLSv1 with cipher ADH-AES256-SHA (256/256 bits)
Jun 25 09:31:22 fnord postfix/smtp[7823]: Trusted TLS connection established to gmail-smtp-in.l.google.com[2a00:1450:4001:c02::1b]:25: TLSv1.2 with cipher ECDHE-RSA-RC4-SHA (128/128 bits)
Jun 25 09:31:22 fnord postfix/smtp[7823]: A1DC9604E6: to=<test-user@gmail.com>, relay=gmail-smtp-in.l.google.com[2a00:1450:4001:c02::1b]:25, delay=1.2, delays=0.19/0.06/0.51/0.41, dsn=2.0.0, status=sent (250 2.0.0 OK 1372152000 xxxxxxxxxxxxxx.xxx - gsmtp)
Jun 25 09:39:17 fnord postfix/smtp[4894]: A0AA260557: to=<test_other@kindle.com>, relay=smtp-fw-4101.amazon.com[72.21.198.25]:25, delay=59, delays=33/0/6.2/20, dsn=2.0.0, status=sent (250 ok:  Message 1220000000 accepted)
`
)

func TestScanner_Scan(t *testing.T) {
	db := NewInMemoryDatabase()

	ch := make(chan string)
	out := []string{}
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for x := range ch {
			out = append(out, x)
		}
		wg.Done()
	}()

	NewScanner(db, ch, nil, nil).Scan(strings.NewReader(scannerTestData))
	close(ch)
	wg.Wait()

	// Check the result.
	if len(out) != 2 || out[0] != "gmail.com" || out[1] != "kindle.com" {
		t.Fatal("Bad result:", out)
	}

	session := db.Session()
	defer session.Close()

	// Check that MX info has been stored in the database.
	var mxinfo MxInfo
	ok := session.Get(mxTable, "gmail-smtp-in.l.google.com", &mxinfo)
	if !ok {
		t.Fatal("No MxInfo for gmail.com")
	}
	if mxinfo.Proto != "TLSv1.2" || mxinfo.Cipher != "ECDHE-RSA-RC4-SHA" {
		t.Fatal("Bad MX info:", mxinfo)
	}

	// Check the domain info in the database.
	mxlist := GetDomainMX(session, "gmail.com")
	if len(mxlist) != 1 || mxlist[0] != "gmail-smtp-in.l.google.com" {
		t.Fatal("Bad MX in DomainMxInfo:", mxlist)
	}

	// Attempt to make the domain -> mx relation expire. Manually
	// tweak the database setting a timestamp in the past.
	old := time.Now().Add(time.Duration(-2) * (*relayTTL))
	session.Set(domainMxTable, "gmail.com:gmail-smtp-in.l.google.com", &old)
	mxlist2 := GetDomainMX(session, "gmail.com")
	if len(mxlist2) > 0 {
		t.Fatal("MX did not expire:", mxlist2)
	}
}

func TestScanner_Whitelist(t *testing.T) {
	db := NewInMemoryDatabase()

	ch := make(chan string)
	out := []string{}
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for x := range ch {
			out = append(out, x)
		}
		wg.Done()
	}()

	wl := ParseWildcardList([]string{"gmail.com", "another-domain.com"})
	NewScanner(db, ch, wl, nil).Scan(strings.NewReader(scannerTestData))
	close(ch)
	wg.Wait()

	// Check the result.
	if len(out) != 1 || out[0] != "gmail.com" {
		t.Fatal("Bad result:", out)
	}
}

func TestScanner_Blacklist(t *testing.T) {
	db := NewInMemoryDatabase()

	ch := make(chan string)
	out := []string{}
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for x := range ch {
			out = append(out, x)
		}
		wg.Done()
	}()

	bl := ParseWildcardList([]string{"kindle.com", "another-domain.com"})
	NewScanner(db, ch, nil, bl).Scan(strings.NewReader(scannerTestData))
	close(ch)
	wg.Wait()

	// Check the result.
	if len(out) != 1 || out[0] != "gmail.com" {
		t.Fatal("Bad result:", out)
	}
}

func TestBatch(t *testing.T) {
	// Create 100 updates for different keys, twice.
	n := 100
	ch := make(chan string)
	go func() {
		for i := 0; i < n; i++ {
			ch <- fmt.Sprintf("%d", i)
		}
		for i := 0; i < n; i++ {
			ch <- fmt.Sprintf("%d", i)
		}
		close(ch)
	}()

	// Expect a single batch with the unique 100 elements.
	outCount := 0
	out := []string{}
	for batch := range Batch(ch, 3600*time.Second) {
		outCount++
		for b, _ := range batch {
			out = append(out, b)
		}
	}

	if outCount != 1 {
		t.Fatal("Too many iterations:", outCount)
	}
	if len(out) != n {
		t.Fatal("Bad number of results:", len(out))
	}
}
