package tlswatch

import (
	"crypto/tls"
	"crypto/x509"
	"log"
	"sync"
)

var (
	tlsProtoNames = map[string]int{
		"SSLv3":   tls.VersionSSL30,
		"TLSv1":   tls.VersionTLS10,
		"TLSv1.1": tls.VersionTLS11,
		"TLSv1.2": tls.VersionTLS12,
	}

	// Database table for the domain info data.
	domainInfoTable = "domain_info"

	// Database table where conflicts are stored.
	conflictsTable = "conflicts"
)

// All the information we have about a specific domain.
type DomainInfo struct {
	// The domain name.
	Domain string

	// List of MX connection information.
	MXs []MxInfo

	// List of unique certificates served by the domain's mail
	// servers.
	Certs []*x509.Certificate

	// Unique trust chains associated with the domain's mail
	// servers.
	TrustChains [][]*x509.Certificate
}

// GetWorstProtocol returns the lowest SSL/TLS protocol version
// supported by all of the MX servers associated with the domain.
func (di DomainInfo) GetWorstProtocol() int {
	protoMin := tls.VersionTLS12 + 1
	for _, mx := range di.MXs {
		if p := tlsProtoNames[mx.Proto]; p < protoMin {
			protoMin = p
		}
	}
	return protoMin
}

// Trusted returns true if all server connections for this domain are
// issued by a CA that belongs to the well-known set (according to
// Postfix configuration).
func (di DomainInfo) Trusted() bool {
	for _, mx := range di.MXs {
		if !mx.Trusted {
			return false
		}
	}
	return true
}

// Equal compares two DomainInfo objects.
func (di DomainInfo) Equal(di2 DomainInfo) bool {
	if di.Domain != di2.Domain {
		return false
	}
	if !certsAreEqual(di.Certs, di2.Certs) {
		return false
	}
	if len(di.TrustChains) != len(di2.TrustChains) {
		return false
	}
	for i := 0; i < len(di.TrustChains); i++ {
		if !certsAreEqual(di.TrustChains[i], di2.TrustChains[i]) {
			return false
		}
	}
	return true
}

func certsAreEqual(a, b []*x509.Certificate) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if !a[i].Equal(b[i]) {
			return false
		}
	}
	return true
}

// Conflicts store old/new information about the TLS profile of a domain.
type Conflict struct {
	Domain string
	OldInfo DomainInfo
	NewInfo DomainInfo
}

// PolicyMap is the final output stage: a map contains a policy entry
// for each domain.
type PolicyMap interface {
	Save() error
	Update(PostfixPolicyEntry)
}

// TlsPolicy creates tls policy entries from the domain mx/certificate
// information.
type TlsPolicy interface {
	CreateEntry(*DomainInfo) (PostfixPolicyEntry, error)
}

// The PolicyWatcher pulls everything together, watching a Scanner
// output for signals and updating a PolicyMap consequently.
type PolicyWatcher struct {
	policy    TlsPolicy
	policyMap PolicyMap
	wg        sync.WaitGroup
	db        Database
	grabber   *CertGrabber
}

func NewPolicyWatcher(policy TlsPolicy, policyMap PolicyMap, db Database, inCh chan UpdateBatch) *PolicyWatcher {
	pw := &PolicyWatcher{
		policy:    policy,
		policyMap: policyMap,
		grabber:   NewCertGrabber(db, 5),
		db:        db,
	}
	pw.wg.Add(1)
	go pw.watchUpdates(inCh)
	return pw
}

// Wait until all the pending requests have completed.
func (pw *PolicyWatcher) Wait() {
	pw.wg.Wait()
	pw.grabber.Close()
	log.Printf("policy watcher exited")
}

// Aggregates all the information we have about a specific domain,
// pulling all related data from the database.
func (pw *PolicyWatcher) makeDomainInfo(session Session, domain string) *DomainInfo {
	mxs := GetDomainMX(session, domain)

	mxInfos := make([]MxInfo, 0, len(mxs))
	allCerts := make([]*x509.Certificate, 0, len(mxs))
	trustChains := make([][]*x509.Certificate, 0, 1)

	for _, mx := range mxs {
		// Get the connection certificates, either from the
		// database cache or directly from the server itself.
		// Errors are ignored, so the resulting DomainInfo
		// will only contain data from successfully validated
		// connections.
		certs, err := pw.grabber.GrabCert(mx)
		if err != nil || len(certs) == 0 {
			continue
		}

		// Aggregate unique certificates.
		serverCert := certs[0]
		certNew := true
		for _, c := range allCerts {
			if serverCert.Equal(c) {
				certNew = false
				break
			}
		}
		if certNew {
			allCerts = append(allCerts, serverCert)
		}

		// Aggregate unique trust chains.
		if len(certs) > 1 {
			trustChain := certs[1 : len(certs)]
			tcNew := true
			for _, tc := range trustChains {
				if certsAreEqual(tc, trustChain) {
					tcNew = false
					break
				}
			}
			if tcNew {
				trustChains = append(trustChains, trustChain)
			}
		}

		// Retrieve connection parameters from the database.
		var mxinfo MxInfo
		if session.Get(mxTable, mx, &mxinfo) {
			mxInfos = append(mxInfos, mxinfo)
		}
	}

	return &DomainInfo{
		Domain:      domain,
		MXs:         mxInfos,
		Certs:       allCerts,
		TrustChains: trustChains,
	}
}

func (pw *PolicyWatcher) watchUpdates(ch chan UpdateBatch) {
	for batch := range ch {
		log.Printf("batch/update: %+v", batch)
		for domain, _ := range batch {
			pw.wg.Add(1)
			go pw.handleDomain(domain)
		}
	}
	pw.wg.Done()
}

func (pw *PolicyWatcher) handleDomain(domain string) {
	session := pw.db.Session()
	defer session.Close()

	// Compute new DomainInfo, and compare it with the existing
	// value in the database. If they differ, a conflict is
	// created.
	info := pw.makeDomainInfo(session, domain)
	var oldInfo DomainInfo
	ok := session.Get(domainInfoTable, domain, &oldInfo)
	if ok && !info.Equal(oldInfo) {
		log.Printf("CONFLICT for %s: %+v -> %+v", domain, oldInfo, info)
		session.Set(conflictsTable, domain, &Conflict{domain, oldInfo, *info})
	} else {
		log.Printf("policy_map: %+v", info)

		entry, err := pw.policy.CreateEntry(info)
		if err == nil {
			pw.policyMap.Update(entry)
		}

		session.Set(domainInfoTable, domain, &info)
	}

	pw.wg.Done()
}

func (pw *PolicyWatcher) handleError(mx string) {
	pw.wg.Done()
}
