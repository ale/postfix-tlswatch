package tlswatch

import (
	"bufio"
	"flag"
	"io"
	"regexp"
	"strings"
	"time"
)

var (
	tlsConnectionRx = regexp.MustCompile(
		`(Trusted|Untrusted) TLS connection established to ([^\[]+)\[([^\]]+)\]:\d+: (.*) with cipher ([-A-Z0-9]*).*$`)
	deliverRx = regexp.MustCompile(
		`to=<[^@]*@([^>]+)>, relay=([^\[]+).* status=sent`)

	// TODO: fix this!
	tlsErrorRx = regexp.MustCompile(
		`TLS error for (.*)`)

	// Command-line flags for TTLs.
	mxSeenTTL = flag.Duration("mx-ttl", time.Hour, "Delay for TLS connection identification")
	relayTTL  = flag.Duration("relay-ttl", 30 * 24 * time.Hour, "TTL for domain -> mx associations")

	// Database tables.
	mxTable       = "mx_info"
	mxSeenTable   = "mx_seen"
	domainMxTable = "domain_mx"
	mxDomainTable = "mx_domain"
)

// MxInfo stores TLS connection information for a mail server.
type MxInfo struct {
	Addr    string
	Trusted bool
	Proto   string
	Cipher  string
}

// A Scanner analyzes a stream of Postfix logs. It has multiple
// purposes:
//
// * It extracts domain -> [mx server] maps for emails that were
//   delivered over opportunistic TLS. Whenever such a delivery
//   is detected, the domain name is sent to the 'outCh' channel.
//   This allows us to keep track of which relations are active,
//   and expire the old ones as they become unused.
//
// * It stores TLS connection parameters for every mail server
//   that was contacted.
//
// * It detects TLS errors. When a TLS connection error to a 'known'
//   mail server is found, the mail server's certificate is
//   invalidated, and all the domains that are related to this mx are
//   updated (via outch). This will cause a new policy computation,
//   which will generate a conflict.
//
// Since the Scanner is stateless (except for the mx tracking), it
// will output domain names repeatedly, once for each delivery; it is
// usually necessary to deduplicate the output using Batch().
//
type Scanner struct {
	db              Database
	outCh           chan string
	domainWhitelist RegexpList
	domainBlacklist RegexpList
}

func NewScanner(db Database, outCh chan string, whitelist RegexpList, blacklist RegexpList) *Scanner {
	return &Scanner{
		db:              db,
		outCh:           outCh,
		domainWhitelist: whitelist,
		domainBlacklist: blacklist,
	}
}

func (s *Scanner) Close() {
}

// Handle a successful TLS connection log.
func (s *Scanner) handleTlsConnection(m []string) {
	session := s.db.Session()
	defer session.Close()
	session.Set(mxTable, m[2], MxInfo{m[2], m[1] == "Trusted", m[4], m[5]})
	session.Set(mxSeenTable, m[2], time.Now())
}

// Handle a TLS connection error.
func (s *Scanner) handleTlsError(m []string) {
	session := s.db.Session()
	defer session.Close()

	// Ignore delivery errors for mail servers to which we have
	// never connected successfully. This has the effect of
	// ignoring errors for domains that we're not managing.
	if !session.Get(mxSeenTable, m[1], nil) {
		return
	}

	// A TLS connection error for a mail server should cause us to
	// refresh the server information as soon as possible. We do
	// so by invalidating the certificate cache data, and by
	// triggering updates for all the domains that have this
	// server as MX.
	session.Del(certsTable, m[1])
	if s.outCh != nil {
		for _, domain := range GetDomainsForMX(session, m[1]) {
			s.outCh <-domain
		}
	}
}

// Handle an email delivery log.
func (s *Scanner) handleDelivery(m []string) {
	if (s.domainWhitelist != nil && !s.domainWhitelist.Match(m[1])) || (s.domainBlacklist != nil && s.domainBlacklist.Match(m[1])) {
		return
	}

	session := s.db.Session()
	defer session.Close()

	// Only add a relay if we've seen the TLS connection first.
	var stamp time.Time
	if !session.Get(mxSeenTable, m[2], &stamp) || time.Since(stamp) > *mxSeenTTL {
		return
	}

	// Store forward & reverse relations.
	stamp = time.Now()
	session.Set(domainMxTable, m[1]+":"+m[2], &stamp)
	session.Set(mxDomainTable, m[2]+":"+m[1], &stamp)

	if s.outCh != nil {
		s.outCh <- m[1]
	}
}

func lastKeyPart(s string) string {
	if pos := strings.LastIndex(s, ":"); pos > 0 {
		return s[pos+1 : len(s)]
	}
	return ""
}

func scanAssociationTable(session Session, table string, prefix string) []string {
	iter, err := session.Scan(table, prefix+":", prefix+";")
	if err != nil {
		return nil
	}
	defer iter.Close()

	result := make([]string, 0)
	for iter.Next() {
		var stamp time.Time
		iter.Value(&stamp)
		if time.Since(stamp) < *relayTTL {
			result = append(result, lastKeyPart(iter.Key()))
		}
	}
	return result
}

// GetDomainMX returns a list of relays for the specified domain. Old
// relays (ones which we haven't talked to in 'relayTTL' time) are
// ignored and automatically expire from the database.
func GetDomainMX(session Session, domain string) []string {
	return scanAssociationTable(session, domainMxTable, domain)
}

// GetDomainsForMX returns the list of domains whose mail is delivered
// through the specified mail server.
func GetDomainsForMX(session Session, mx string) []string {
	return scanAssociationTable(session, mxDomainTable, mx)
}

// HandleLog parses a single log line (it tries its best to ignore the
// actual syslog format used). Note that since no attempts are made at
// parsing the log timestamp, the current time is always used -- this
// means that if we're batch processing a large log file, expirations
// won't work as expected (not a big deal anyway).
func (s *Scanner) HandleLog(line string) {
	if !strings.Contains(line, "postfix/") {
		return
	}

	if m := tlsConnectionRx.FindStringSubmatch(line); m != nil {
		s.handleTlsConnection(m)
	} else if m := tlsErrorRx.FindStringSubmatch(line); m != nil {
		s.handleTlsError(m)
	} else if m := deliverRx.FindStringSubmatch(line); m != nil {
		s.handleDelivery(m)
	}
}

// Scan consumes lines from an io.Reader.
func (s *Scanner) Scan(r io.Reader) error {
	scanner := bufio.NewScanner(r)
	lines := 0
	for scanner.Scan() {
		s.HandleLog(scanner.Text())
		lines++
	}
	return scanner.Err()
}

// UpdateBatch is simply a set of unique strings.
type UpdateBatch map[string]struct{}

type batcher struct {
	inCh    chan string
	outCh   chan UpdateBatch
	updates UpdateBatch
}

// Batch deduplicates 'chan string' updates and outputs the aggregated
// update on another channel with the desired periodicity. Closing the
// input channel will cause the output one to be closed as well.
func Batch(inCh chan string, period time.Duration) chan UpdateBatch {
	um := &batcher{
		inCh:    inCh,
		outCh:   make(chan UpdateBatch, 1),
		updates: make(UpdateBatch),
	}
	// Limit update rate to once a minute.
	go um.run(period)

	return um.outCh
}

func (um *batcher) flush() {
	if len(um.updates) > 0 {
		um.outCh <- um.updates
		um.updates = make(UpdateBatch)
	}
}

func (um *batcher) run(period time.Duration) {
	t := time.NewTicker(period)
	for {
		select {
		case s := <-um.inCh:
			if s == "" {
				um.flush()
				close(um.outCh)
				return
			}
			um.updates[s] = struct{}{}
		case <-t.C:
			um.flush()
		}
	}
}
