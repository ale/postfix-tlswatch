
++++++++
tlswatch
++++++++

tlswatch is a tool to enforce a STARTTLS policy for your mail server,
combining opportunistic TLS discovery and a "sticky"
trust-on-first-use policy. It currently only supports Postfix.

tlswatch will scan your email logs looking for remote mail servers
that support TLS connections, and it will attempt to establish a trust
path to them. If it succeeds, it will generate a new Postfix TLS
policy to lock the trust path for the destination domain.


TLS policy
----------

There are two available enforcement policies:

*strict*
  This policy will match the exact fingerprints of the mail servers'
  X509 certificates. While more precise, it will generate lots of
  conflicts on every key rotation, so it is advised to use it only if
  an independent validation mechanism is available.

*ca-pinning*
  This policy will match certificates against the top-level signing
  CA. For this to work, the remote mail server must send the full
  certificate chain on every TLS connection (in case the CA is not
  part of the set of well-known authorities distributed with the
  system).

It is also possible to restrict policies to a set of manually
specified domains, or to provide a blacklist of domains to exclude.


Key rotation and error reconciliation
-------------------------------------

When a TLS connection error occurs, a decision must be made as to the
whether change is legitimate or not. Since email to the destination
domain will be queued until the condition is resolved, we'd like to
attempt to validate the change automatically. A few mechanisms come to
mind:

* DANE/TLSA to exploit the independent DNSSEC-based channel
* PGP web-of-trust

Manual resolution is always available as a last resort.
