package tlswatch

import (
	"bytes"
	"encoding/gob"
	"errors"
	"sync"
)

// InMemoryDatabase, as one would expect, only stores its data in
// memory. Useful for testing.
type InMemoryDatabase struct {
	lock   sync.Mutex
	tables map[string]map[string][]byte
}

func NewInMemoryDatabase() *InMemoryDatabase {
	return &InMemoryDatabase{
		tables: make(map[string]map[string][]byte),
	}
}

func (db *InMemoryDatabase) Close() {
}

func (db *InMemoryDatabase) Session() Session {
	return &inmemorySession{db}
}

type inmemoryKV struct {
	key   string
	value []byte
}

type inmemoryIterator struct {
	values []inmemoryKV
	pos    int
}

func (i *inmemoryIterator) Next() bool {
	i.pos++
	return i.pos < len(i.values)
}

func (i *inmemoryIterator) Key() string {
	return i.values[i.pos].key
}

func (i *inmemoryIterator) Value(obj interface{}) {
	gob.NewDecoder(bytes.NewReader(i.values[i.pos].value)).Decode(obj)
}

func (i *inmemoryIterator) Close() {
}

type inmemorySession struct {
	db *InMemoryDatabase
}

func (s *inmemorySession) Get(table, key string, obj interface{}) bool {
	s.db.lock.Lock()
	defer s.db.lock.Unlock()

	if t, ok := s.db.tables[table]; ok {
		if value, ok := t[key]; ok {
			if obj != nil {
				gob.NewDecoder(bytes.NewReader(value)).Decode(obj)
			}
			return true
		}
	}
	return false
}

func (s *inmemorySession) Set(table, key string, obj interface{}) {
	s.db.lock.Lock()
	defer s.db.lock.Unlock()

	var buf bytes.Buffer
	gob.NewEncoder(&buf).Encode(obj)

	t, ok := s.db.tables[table]
	if !ok {
		t = make(map[string][]byte)
		s.db.tables[table] = t
	}
	t[key] = buf.Bytes()
}

func (s *inmemorySession) Del(table, key string) {
	s.db.lock.Lock()
	defer s.db.lock.Unlock()
	if t, ok := s.db.tables[table]; ok {
		delete(t, key)
	}
}

func (s *inmemorySession) Scan(table, startKey, endKey string) (Iterator, error) {
	t, ok := s.db.tables[table]
	if !ok {
		return nil, errors.New("No such table")
	}

	values := make([]inmemoryKV, 0)
	for key, value := range t {
		if key >= startKey && key < endKey {
			values = append(values, inmemoryKV{key, value})
		}
	}

	return &inmemoryIterator{values, -1}, nil
}

func (s *inmemorySession) Close() {
}
