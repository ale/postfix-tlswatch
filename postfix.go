package tlswatch

// Methods and types for interfacing with the Postfix MTA.

import (
	"crypto/x509"
	"encoding/pem"
	"flag"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"errors"
	"crypto/tls"
)

var (
	trustAnchorPath = flag.String("trust-anchor-path", "/etc/postfix/trust-anchors", "Where to store trust anchor (CA) certificates")
)

// Trust anchor. Since we need the certificates to be store in a file,
// we use a filename that is unique to the specific certificate chain
// (using their SHA fingerprints).
type PostfixTrustAnchor struct {
	Id    string
	Certs []*x509.Certificate
}

var (
	// Keep a global cache of trust anchors in memory, so that
	// lookups for multiple domains are fast.
	taCache     map[string]*PostfixTrustAnchor
	taCacheLock sync.Mutex
)

// Create a new PostfixTrustAnchor using the given certificates.
// Actual files on the filesystem are only created once (with a unique
// file name generated using the SHA1 fingerprints of the
// certificates). Multiple calls with identical arguments will return
// the same pointer.
func NewPostfixTrustAnchor(certs []*x509.Certificate) *PostfixTrustAnchor {
	fps := make([]string, 0, len(certs))
	for _, cert := range certs {
		fps = append(fps, strings.Replace(GetCertFingerprint(cert), ":", "", -1))
	}
	id := strings.Join(fps, "_")

	taCacheLock.Lock()
	defer taCacheLock.Unlock()

	// Lazy cache initialization.
	if taCache == nil {
		taCache = make(map[string]*PostfixTrustAnchor)
	}

	if ta, ok := taCache[id]; ok {
		return ta
	}
	ta := &PostfixTrustAnchor{
		Id:    id,
		Certs: certs,
	}
	// Ensure that the file exists, create it otherwise.
	ta.save()
	taCache[id] = ta
	return ta
}

func (ta *PostfixTrustAnchor) Filename() string {
	return filepath.Join(*trustAnchorPath, ta.Id+".pem")
}

func (ta *PostfixTrustAnchor) save() error {
	if _, err := os.Stat(ta.Filename()); err == nil {
		return nil
	}

	file, err := os.Create(ta.Filename())
	if err != nil {
		return err
	}
	defer file.Close()

	for _, cert := range ta.Certs {
		if err := pem.Encode(file, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Raw}); err != nil {
			return err
		}
	}

	return nil
}

// An entry in the Postfix tls_policy_map.
type PostfixPolicyEntry struct {
	Domain       string
	Policy       string
	Ciphers      string
	Protocols    string
	Match        []string
	TrustAnchors []*PostfixTrustAnchor
}

func formatNicely(words []string) string {
	out := ""
	pos := 0
	maxSz := 30
	sep := "\n                              "
	for i, arg := range words {
		pos += len(arg)
		if i > 0 {
			if pos > maxSz {
				out += sep
				pos = len(arg)
			} else {
				out += " "
			}
		}
		out += arg
	}
	return out
}

func (e PostfixPolicyEntry) String() string {
	args := []string{e.Policy}
	if e.Ciphers != "" {
		args = append(args, fmt.Sprintf("ciphers=%s", e.Ciphers))
	}
	if e.Protocols != "" {
		args = append(args, fmt.Sprintf("protocols=%s", e.Protocols))
	}
	for _, ta := range e.TrustAnchors {
		args = append(args, fmt.Sprintf("tafile=%s", ta.Filename()))
	}
	for _, m := range e.Match {
		args = append(args, fmt.Sprintf("match=%s", m))
	}
	return fmt.Sprintf("%-29s %s\n\n", e.Domain, formatNicely(args))
}

// PostfixPolicyMap is an abstract representation of the Postfix
// tls_policy_map, in the format described in
// http://www.postfix.org/postconf.5.html#smtp_tls_policy_maps
type PostfixPolicyMap struct {
	path string
	m    map[string]PostfixPolicyEntry
	lock sync.Mutex
}

func NewPostfixPolicyMap(path string) *PostfixPolicyMap {
	return &PostfixPolicyMap{
		path: path,
		m:    make(map[string]PostfixPolicyEntry),
	}
}

func (pm *PostfixPolicyMap) Update(entry PostfixPolicyEntry) {
	pm.lock.Lock()
	defer pm.lock.Unlock()
	pm.m[entry.Domain] = entry
}

func (pm *PostfixPolicyMap) encode(w io.Writer) error {
	pm.lock.Lock()
	defer pm.lock.Unlock()
	for _, entry := range pm.m {
		if _, err := io.WriteString(w, entry.String()); err != nil {
			return err
		}
	}
	return nil
}

func (pm *PostfixPolicyMap) encodeToFile() error {
	file, err := os.Create(pm.path)
	if err != nil {
		return err
	}
	defer file.Close()
	return pm.encode(file)
}

func (pm *PostfixPolicyMap) Save() error {
	if err := pm.encodeToFile(); err != nil {
		return err
	}
	return exec.Command("postmap", pm.path).Run()
}

// The 'strict' policy verifies the fingerprints of the mail server
// certificates.
type StrictTlsPolicy struct{}

func (p StrictTlsPolicy) CreateEntry(info *DomainInfo) (PostfixPolicyEntry, error) {
	if len(info.Certs) == 0 {
		return PostfixPolicyEntry{}, errors.New("No certificates found")
	}

	entry := PostfixPolicyEntry{
		Domain:    info.Domain,
		Policy:    "fingerprint",
		Match:     make([]string, 0, len(info.Certs)),
		Protocols: guessProtocols(info),
	}
	for _, cert := range info.Certs {
		entry.Match = append(entry.Match, GetCertFingerprint(cert))
	}
	return entry, nil
}

// The 'ca-pinning' policy verifies the certificate using the
// specified trust anchors.
type CaPinningTlsPolicy struct{}

func (p CaPinningTlsPolicy) CreateEntry(info *DomainInfo) (PostfixPolicyEntry, error) {
	if len(info.Certs) == 0 {
		return PostfixPolicyEntry{}, errors.New("No certificates found")
	}
	if len(info.TrustChains) == 0 {
		return PostfixPolicyEntry{}, errors.New("No trust chains provided with the certificates on the TLS connection")
	}

	entry := PostfixPolicyEntry{
		Domain:       info.Domain,
		Policy:       "verify",
		TrustAnchors: make([]*PostfixTrustAnchor, 0, len(info.TrustChains)),
		Protocols:    guessProtocols(info),
	}
	for _, trustChain := range info.TrustChains {
		ta := NewPostfixTrustAnchor(trustChain)
		entry.TrustAnchors = append(entry.TrustAnchors, ta)
	}
	return entry, nil
}

// Return a TlsPolicy by name.
func GetTlsPolicy(name string) (TlsPolicy, error) {
	switch name {
	case "ca-pinning":
		return &CaPinningTlsPolicy{}, nil
	case "strict":
		return &StrictTlsPolicy{}, nil
	}
	return nil, errors.New("Unknown policy")
}

// Set the 'protocols' field in the TLS policy entry depending on the
// protocols that the MX servers support. This effectively prevents
// downgrading to lower (unsafe) protocols for those servers that
// supports higher versions of TLS.
func guessProtocols(info *DomainInfo) string {
	switch info.GetWorstProtocol() {
	case tls.VersionTLS10:
		return "TLSv1,TLSv1.1,TLSv1.2"
	case tls.VersionTLS11:
		return "TLSv1.1,TLSv1.2"
	case tls.VersionTLS12:
		return "TLSv1.2"
	}
	return ""
}
