package tlswatch

import (
	"testing"
	"strings"
)

func TestWildcards(t *testing.T) {
	s := `
# test data
autistici.org
*.inventati.org
`

	rl := ParseWildcardsFromStream(strings.NewReader(s))
	if len(rl) != 2 {
		t.Fatal("Bad parsed result:", rl)
	}

	testData := []struct{
		data string
		expected bool
	}{
		{"unknown.domain", false},
		{"autistici.org", true},
		{"subdomain.autistici.org", false},
		{"inventati.org", true},
		{"subdmoain.inventati.org", true},
	}

	for _, td := range testData {
		result := rl.Match(td.data)
		if result != td.expected {
			t.Errorf("Match() failed for '%s' (%+v, expected %+v)", td.data, result, td.expected)
		}
	}
}
